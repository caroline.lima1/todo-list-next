import { useEffect, useState } from 'react';

export default function todoList() {
    const [todos, setTodos] = useState(null);
    const [todo, setTodo] = useState('');
    const [newId, setNewId] = useState('');

    function handleSubmit(){
        fetch(`http://localhost:8000/todos`, 
            {
                method: 'post',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                   },
                body: JSON.stringify({
                    id: newId,
                    title: todo,
                    done: false,
                })
            })
        .then(async res => {
            const data = await res.json();
        })
        window.location.reload()
    }

    function changeTodo(id, title, done){
        fetch(`http://localhost:8000/todos/${id}`, 
            {
                method: 'put',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                   },
                body: JSON.stringify({
                    id: id,
                    title: title,
                    done: !done
                })
            })
        .then(async res => {
            const data = await res.json();
        })
        window.location.reload()
    }

    function removeTodo(id) {
      fetch(`http://localhost:8000/todos/${id}`, {
          method: 'delete'
      })
      .then(res =>
        res.json().then(json => {
            return json;
        })
      );
      window.location.reload()
    }

    useEffect(() => {
      fetch('http://localhost:8000/todos')
        .then(async res => {
            const data = await res.json();
            console.log(data) //aqui ó
            setTodos(data)
            setNewId(data.length)
        })
    }, []);

    return (
        <>  
            <h1 className={`flex w-4/5 justify-center p-4 mt-5 items-center font-bold text-2xl text-blue-900`}>Todo List</h1>
            <div className={`flex w-4/5 justify-center p-4 mb-2 items-center`} >
                <input className={"w-5/12 border-2 border-blue-300 hover:border-blue-700 rounded h-10"} value={todo} onChange={(e) => setTodo(e.target.value)} />
                <button className={"ml-6 text-blue-300 text-xl rounded border-2 h-10 w-20 border-blue-300 hover:bg-blue-100 "} onClick={handleSubmit}>Add</button>
            </div>
                {todos ? todos.map((todo, index) => (
            <div key={index} className={`flex w-4/5 justify-center items-center`} >
                <div className={`flex w-3/5 p-4 mb-2 justify-between items-center rounded ${todo.done ? "bg-gradient-to-r from-blue-100 to-white" : "bg-gradient-to-r from-blue-300 to-blue-100"}`} 
                key={index}>
                    <h1 className={`ml-9 text-xl cursor-pointer ${todo.done ? "text-blue-300 line-through" : "text-blue-900"}`} 
                    key={todo.id} onClick={() => changeTodo(todo.id, todo.title, todo.done)}>{todo.title}</h1>
                    <div className="w-1/6 flex justify-between items-center">
                        <button className="h-7 w-7 mr-10 flex justify-center items-center bg-blue-400 hover:bg-blue-900 text-white font-bold rounded" onClick={() => removeTodo(todo.id)}>X</button>
                    </div>
                </div>
            </div>
            )) : <><h1>No Items</h1></>}
        </>
    )
}