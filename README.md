# Todo List Next

## Features
- JS
- Next
- Json Server
- Tailwind CSS

## Usage 

### Start
- Clone this repo to your desktop and run npm install to install all the dependencies
- Once the dependencies are installed, you can run npm run dev to start the application and access it at localhost:3000. For json server run npm run json:server.

